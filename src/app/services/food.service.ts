import { Injectable } from '@angular/core';
import { Food } from '../models/food.model';

@Injectable({
  providedIn: 'root',
})
export class FoodService {
  getFoods(): Food[] {
    return [
      {
        id: 1,
        title: 'Plumber',
        price: 500,
        image: 'assets/images/foods/plumber.jpg',
        description:
          'Assemble, install, maintain, and pressure test all pipes, fittings, and fixtures of heating, water, drainage, sprinkler, and gas systems according to specifications and plumbing codes.',
      },
      {
        id: 2,
        title: 'Electrician',
        price: 500,
        image: 'assets/images/foods/electrician.jpg',
        description:
          'Responsible for inspecting, testing, repairing, installing, and modifying electrical components and systems. Electricians general work at homes, businesses, and construction sites, and generally work as contractors.',
      },
      {
        id: 3,
        title: 'Painter',
        price: 400,
        image: 'assets/images/foods/painter.jpg',
        description:
          'Preparing painting surfaces by washing walls, repairing holes, or removing old paint. Mixing, matching, and applying paints and other finishes to various surfaces. Providing decorative and faux finishes as the project requires.',
      },
      {
        id: 4,
        title: 'Handyman',
        price: 600,
        image: 'assets/images/foods/handyman.jpg',
        description:
          'Conducts basic maintenance on various businesses or homes. Their main duties include repairing plumbing systems, fixing company equipment or tools and testing various company or home appliances to ensure they work properly.',
      },
      {
        id: 5,
        title: 'Manicurist',
        price: 400,
        image: 'assets/images/foods/handyman.jpg',
        description:
          'Work exclusively on grooming and painting fingernails. Nail grooming includes cleaning and cutting both fingernails and cuticles.',
      },
      {
        id: 6,
        title: 'AC Repair',
        price: 500,
        image: 'assets/images/foods/electrician.jpg',
        description:
          'The air conditioning technicians install, inspect, maintain, and repair air conditioner. They ventilate equipment and controls making sure they operate efficiently and continuously.',
      },
        
    ];
  }

  getFood(id: number): Food {
    return this.getFoods().find((food) => food.id === id);
  }
}
