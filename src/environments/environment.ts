// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCoyPnBSQTBBGkPcsHlcOdSN8acB7qU5mY",
    authDomain: "hero-services-6976a.firebaseapp.com",
    databaseURL: "https://hero-services-6976a-default-rtdb.firebaseio.com",
    projectId: "hero-services-6976a",
    storageBucket: "hero-services-6976a.appspot.com",
    messagingSenderId: "808775600774",
    appId: "1:808775600774:web:f1d037489632733f089f07"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
